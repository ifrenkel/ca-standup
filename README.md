# ca-standup

This project is a cli tool for posting standup updates in slack. It uses the gitlab api to get a list of issues assigned to a user and prompt for updates on these.

## Getting started

A `read_api` access token is needed.

## How to use

1. put access token in .env
    ```
   GL_ACCESS_TOKEN=<TOKEN>
   ```
2. run the tool and fill out updates when prompted
3. output with proper markdown
4. copy/paste

## Markdown

In order for the markdown to be picked up, slack has to be configured correctly.  Go to `Preferences > Advanced` and select `Format messages with markup`.

