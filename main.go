package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/xanzy/go-gitlab"
)

func prompt() (string, error) {
	r := bufio.NewReader(os.Stdin)
	c := 0
	var res string
	for {
		s, err := r.ReadString('\n')
		if err != nil {
			return "", err
		}
		if s == ".\n" {
			break
		}
		res += s
		c++
		if c > 3 {
			break
		}
	}
	return res, nil
}

func getsel() (string, error) {
	r := bufio.NewReader(os.Stdin)
	s, err := r.ReadString('\n')
	if err != nil {
		return "", err
	}
	return strings.Trim(s, "\n"), nil
}

type issuereport struct {
	issue  *gitlab.Issue
	report string
}

func (ir *issuereport) updated() bool {
	return len(ir.report) > 0
}

type reportlist struct {
	reports []*issuereport
}

func (rl reportlist) String() string {
	var res string
	for idx, ir := range rl.reports {
		if ir.updated() {
			res += fmt.Sprintf("%d. `%s` (%s)\n", idx, ir.issue.Title, ir.issue.WebURL)
			s := strings.Replace(ir.report, "*", "    *", -1)
			res += fmt.Sprintf("%s\n", s)
		}
	}
	return res
}

func newReportList(issues []*gitlab.Issue) reportlist {
	res := reportlist{
		reports: make([]*issuereport, 0),
	}
	for _, issue := range issues {
		ir := issuereport{issue: issue}
		res.reports = append(res.reports, &ir)
	}
	return res
}

// user's slack has to have markup turned off in slack
// :ci_passing:
// bullets have to be 4 spaces
func main() {
	env, err := os.ReadFile(".env")
	if err != nil {
		log.Fatalf("Failed to get access token %v", err)
	}

	var token string
	lines := strings.Split(string(env), "\n")
	for _, l := range lines {
		parts := strings.Split(l, "=")
		if parts[0] == "GL_ACCESS_TOKEN" {
			token = parts[1]
		}
	}

	gl, err := gitlab.NewClient(token)
	if err != nil {
		log.Fatalf("Failed to initialize client %v", err)
	}

	issues, err := getissues(gl)
	if err != nil {
		log.Fatalf("Failed to fetch issues %v", err)
	}

	fmt.Println("Select issue for update:")
	rl := newReportList(issues)

	for {
		for idx, ir := range rl.reports {
			fmt.Printf("%d: %s\n", idx, ir.issue.Title)
		}
		fmt.Println("q: Quit")

		sel, err := getsel()
		if err != nil {
			log.Fatalf("Error getting selection %v", err)
		}

		if sel == "q" {
			break
		}

		n, err := strconv.Atoi(sel)
		if err != nil {
			log.Fatalf("Selection should be an int %v", err)
		}

		if n < 0 || n > len(rl.reports)-1 {
			log.Fatalf("Invalid selection %d", n)
		}
		ir := rl.reports[n]

		fmt.Printf("Enter update for `%s`\n", ir.issue.Title)
		fmt.Println("  (Enter `.` to finish update.)")
		update, err := prompt()
		if err != nil {
			log.Fatalf("Error getting update %v", err)
		}
		ir.report = update
	}

	err = save(rl)
	if err != nil {
		log.Fatalf("Error saving report %v", err)
	}

	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	err = cmd.Run()
	if err != nil {
		log.Fatalf("Error clearing screen %v", err)
	}

	fmt.Println("Markdown-formatted update (saved to .last_update.txt):")
	fmt.Println(rl.String())
}

const saveFile = ".last_update.txt"

func save(rl reportlist) error {
	return os.WriteFile(saveFile, []byte(rl.String()), 0644)
}

func getissues(gl *gitlab.Client) ([]*gitlab.Issue, error) {
	var state = "opened"
	opts := gitlab.ListIssuesOptions{
		State: &state,
		Scope: gitlab.Ptr("assigned_to_me"),
	}

	issues, _, err := gl.Issues.ListIssues(&opts)
	if err != nil {
		return nil, err
	}

	return issues, nil
}

func mrs(gl *gitlab.Client) {
	opts := gitlab.ListMergeRequestsOptions{
		State: gitlab.Ptr("opened"),
		Scope: gitlab.Ptr("assigned_to_me"),
	}
	mrs, _, err := gl.MergeRequests.ListMergeRequests(&opts)
	if err != nil {
		log.Fatalf("Failed to fetch issues %v", err)
	}
	for _, mr := range mrs {
		fmt.Println(mr.IID, mr.Title)
	}

}

func todos(gl *gitlab.Client) {
	opts := gitlab.ListTodosOptions{
		State: gitlab.Ptr("pending"),
	}
	todos, _, err := gl.Todos.ListTodos(&opts)
	if err != nil {
		log.Fatalf("Failed to fetch todos %v", err)
	}
	for _, t := range todos {
		fmt.Println(t.ID, t.ActionName, t.TargetType)
	}
}
