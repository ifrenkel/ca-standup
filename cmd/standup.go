package main

import (
	"fmt"
	"log"
)

// (1) show welcome screen
// (2) auth path
// (1) query issues
// (2) query mrs
// (3) query todos
// (2) error path
// (1) present list of issues
// (2) present list of mrs
// (3) present list of todos
// {1) prompt for issue to report on
// (2) add mrs to prompt
// (3) add todos to prompt
// (1) get input text
// (1) ask done
// (1) present markdown
// (2) if not done, show issue list again with reported removed
// (2) add other for freeform entry

func main() {
	cl := api.NewClient()
	standup.WelcomeScreen()
	issues, err := cl.fetchIssues()
	if err != nil {
		log.Fatal(err)
	}
	report, err := standup.IssuePrompt(issues)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(report.String())
}
